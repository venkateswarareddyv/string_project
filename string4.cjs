function fullName(givenObject){
    let newString="";
    for(let keys in givenObject){
        let value=givenObject[keys]
        let ValueZeroIndex=value[0]
        let remainingString=value.slice(1,value.length);
        let titleCase=ValueZeroIndex.toUpperCase()+remainingString.toLowerCase();
        newString +=titleCase+" "
    }
    return newString;
    
}

module.exports=fullName;
