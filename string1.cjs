function convertToNumber(givenString){
    if(typeof(givenString)==="string"){
        if(givenString.replace(/[0-9$.-]/gi,"").length !==0){
            return 0;
        }else{
            return parseFloat(givenString.replace('$',""));
        }
    }else{
        return 0;
    }
}

module.exports=convertToNumber;

